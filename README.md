# sky-automation


#### A TestCafe Typescript framework for sky automation.
These test are written using typescript and Tescafe Tool.

**Prerequisites**
  - Node 12

**Installing**
```sh
 nvm use
```
**Running Tests With Browser Visible:**
```sh
 npm run <browser>
```
e.g. `npm run chrome`
***

**Running Tests With Browser Hidden:**
```sh
 npm run <browser>:headless
```
e.g. `npm run chrome:headless`
***

**View test run report in below file in project folder**
```sh
test-results.xml
```


